//*****************************************************************************
// File:				main.cpp
// Author:			Clio Skevington
// Date:				November x 2016
// Class:				CS 150-01
// Assignment:	x
// Purpose:			x
// Hours:				x
//*****************************************************************************
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <ctime>
#include <cmath>
#include "car.h"

using namespace std;

const int MAX_LEN = 31668;
const int INFO = 3;
const int MAX_VELOCITY = 296;
ofstream outFile;
const string OUTPUT = "data.txt";
const int I5_1 = 7917;
const int I5_2 = 15834;
const int I5_3 = 23751;
const int I5_4 = 31668;
const int I5_LEN = 31668;
const int I90_1 = 2064;
const int I90_2 = 4128;
const int I90_3 = 6192;
const int I90_4 = 8257;
const int I90_LEN = 8257;
const int I405_1 = 2227;
const int I405_2 = 4454;
const int I405_3 = 6681;
const int I405_4 = 8908;
const int I405_LEN = 8908;
const int S520_1 = 939;
const int S520_2 = 1878;
const int S520_3 = 2817;
const int S520_4 = 3758;
const int S520_LEN = 3758;
const int I5 = 5;
const int I90 = 90;
const int I405 = 405;
const int S520 = 520;
const int NUM_GATES = 4;
const double NUM_TRIALS = 18;

void fillLane(Car lane[MAX_LEN], int len, int peopleCars, int selfCars, int totalCars);
bool hasACar(Car lane[MAX_LEN], int pos);
int checkVelocity(Car lane[MAX_LEN], int pos);
bool checkSelfDriving(Car lane[MAX_LEN], int pos);
int randGen(int max);
void updateCar(Car lane[MAX_LEN], int pos, int len);
void moveCar(Car lane[MAX_LEN], int len, int flowTotals[NUM_GATES], int routeNum);
int randSpeed(int min, int max);
double calcDensity(Car lane[MAX_LEN], int gate, int routeNum);
double calcFlow(int flowTotal);
double calcAvgSpd(Car lane[MAX_LEN], int gate, int routeNum);
void trackFlow(int start, int end, int flowTotals[NUM_GATES], int routeNum);
void initializeFlow(int flowTotals[NUM_GATES]);
int getMarker(int gateNum, int routeNum);
int getGateMarker(int gateNum);
double calcTot(Car lane[MAX_LEN], int routeNum, double &total);
int getRoadLength(int routeNum);
double calcAvgTot(double total, double count);
void emptyLanes(Car lane[MAX_LEN], int length);
void multipleFill(Car lane[5][S520_LEN], int len, int peopleCars, int selfCars, int totalCars, int numLanes);


int main()
{
	const double ACCIDENT = .0001526;
	int peopleCars;
	int selfCars;
	double selfCarsNum;
	int routeNum;
	int routeLen;
	int totalCars;
	int steps;
	int flowTotals[NUM_GATES];
	double selfCarsPercent;
	double total = 0;
	double finalTotal = 0;
	double density = 0;
	double flow = 0;
	Car lanes[5][S520_LEN];
	//Car lane[MAX_LEN];
	//row 1 is car(1) or empty(0), row 2 is velocity, row 3 is self(1) or people(0) car

	cout << fixed << setprecision (4);
	srand (static_cast<unsigned> (time (static_cast<time_t *>(NULL))));

	outFile.open(OUTPUT);

	cout << "Enter route number: ";
	cin >> routeNum;
	cout << "Enter total cars: ";
	cin >> totalCars;

	selfCarsPercent = 0;
	/*PAST SIMULATIONS
	cout << "Enter percent of self-driven cars (where 1 = 100%): ";
	cin >> selfCarsPercent;
	cout << "Enter number of steps to go through: ";
	cin >> steps;
	PAST SIMULATIONS*/

//---LOAD SIMULATOR----------------------------------------------------------//

	steps = 60;

	if (routeNum == I5)
	{
		routeLen = I5_LEN;
	}
	else if (routeNum == I90)
	{
		routeLen = I90_LEN;
	}
	else if (routeNum == I405)
	{
		routeLen = I405_LEN;
	}
	else if (routeNum == S520)
	{
		routeLen = S520_LEN;
	}
	else
	{
		routeLen = 50; //for testing
	}

//----RUN SET OF SIMULATIONS (5x)-------------------------------------------//
	for (int setRuns = 1; setRuns <= 5; setRuns++)
	{
		//selfCarsPercent = static_cast <double> (setRuns) / 10; PREV SIM

		/*FLOW-DENSITY SIM
		switch (setRuns)
		{
		case 0: selfCarsPercent = 0;
			break;
		case 1: selfCarsPercent = .1;
			break;
		case 2: selfCarsPercent = .5;
			break;
		case 3: selfCarsPercent = .9;
			break;
		}

		cout << "Set Percent is: " << selfCarsPercent << endl; 
		FLOW-DENSITY SIM*/

		selfCarsNum = selfCarsPercent * totalCars;
		selfCars = static_cast <int> (selfCarsNum);
		peopleCars = totalCars - selfCars;

//----RUN SIMULATIONS (3x) --------------------------------------------------//
		for (int runs = 1; runs <= 3; runs++)
		{
			//fillLane(lane, routeLen, peopleCars, selfCars, totalCars); PREV SIM
			multipleFill(lanes, routeLen, peopleCars, selfCars, totalCars, setRuns);
			initializeFlow(flowTotals);
			density = 0;
			flow = 0;
			total = 0;

			//----RUN A SINGLE SIMULATION -----------------------------------------------//
			for (int i = 0; i <= steps; i++)
			{
				//cout << " Check 1: " << calcAvgSpd(lane, 1, I5);

				for (int laneNum = 0; laneNum < setRuns; laneNum++)
				{
					for (int j = 0; j < routeLen; j++)
					{
						if (hasACar(lanes[laneNum], j))
						{
							updateCar(lanes[laneNum], j, routeLen);
						}
					}
				}

				/*SINGLE-LANE
				for (int j = 0; j < routeLen; j++)
				{
					if (hasACar(lane, j))
					{
						updateCar(lane, j, routeLen);
					}
				} */

				//cout << " Check 2: " << calcAvgSpd(lane, 1, I5);

				//moveCar(lane, routeLen, flowTotals, routeNum); - single-lane

				for (int laneNum = 0; laneNum < setRuns; laneNum++)
				{
					moveCar(lanes[laneNum], routeLen, flowTotals, routeNum);
				}

				//cout << " Check 3: " << calcAvgSpd(lane, 1, I5) << endl;

				//OUTPUT EVERY 10 MINUTES--------------------------------------------//
				if (i % 10 == 0 && i > 0)
				{
					cout << i << " MINUTE MARK\n";
					/* PREV SIMULATIONS
					cout << "\nDensity for section 1 is " << calcDensity(lane, 1, routeNum);
					cout << "\nDensity for section 2 is " << calcDensity(lane, 2, routeNum);
					cout << "\nDensity for section 3 is " << calcDensity(lane, 3, routeNum);
					cout << "\nDensity for section 4 is " << calcDensity(lane, 4, routeNum);
					cout << "\nFlow for gate 1 is " << calcFlow(flowTotals[0]);
					cout << "\nFlow for gate 2 is " << calcFlow(flowTotals[1]);
					cout << "\nFlow for gate 3 is " << calcFlow(flowTotals[2]);
					cout << "\nFlow for gate 4 is " << calcFlow(flowTotals[3]);
					cout << "\nAverage Speed for section 1 is " << calcAvgSpd(lane, 1, routeNum);
					cout << "\nAverage Speed for section 2 is " << calcAvgSpd(lane, 2, routeNum);
					cout << "\nAverage Speed for section 3 is " << calcAvgSpd(lane, 3, routeNum);
					cout << "\nAverage Speed for section 4 is " << calcAvgSpd(lane, 4, routeNum);

					outFile << calcDensity(lane, 1, routeNum) << " ";
					outFile << calcDensity(lane, 2, routeNum) << " ";
					outFile << calcDensity(lane, 3, routeNum) << " ";
					outFile << calcDensity(lane, 4, routeNum) << " ";
					outFile << calcFlow(flowTotals[0]) << " ";
					outFile << calcFlow(flowTotals[1]) << " ";
					outFile << calcFlow(flowTotals[2]) << " ";
					outFile << calcFlow(flowTotals[3]) << " ";
					outFile << endl;

					initializeFlow(flowTotals); 
					calcTot(lane, routeNum, total);
					density += calcDensity(lane, 1, routeNum);
					density += calcDensity(lane, 2, routeNum);
					density += calcDensity(lane, 3, routeNum);
					density += calcDensity(lane, 4, routeNum);
					flow += calcFlow(flowTotals[0]);
					flow += calcFlow(flowTotals[1]);
					flow += calcFlow(flowTotals[2]);
					flow += calcFlow(flowTotals[3]); */

					for (int laneNum = 0; laneNum < setRuns; laneNum++)
					{
						calcTot(lanes[laneNum], routeNum, total);
						density += calcDensity(lanes[laneNum], 1, routeNum);
						density += calcDensity(lanes[laneNum], 2, routeNum);
						density += calcDensity(lanes[laneNum], 3, routeNum);
						density += calcDensity(lanes[laneNum], 4, routeNum);
						flow += calcFlow(flowTotals[0]);
						flow += calcFlow(flowTotals[1]);
						flow += calcFlow(flowTotals[2]);
						flow += calcFlow(flowTotals[3]);
					}

					initializeFlow(flowTotals);
				}
				//10-MINUTE MARKER---------------------------------------------------//
			}
//----SINGLE SIMULATION OVER-------------------------------------------------//

			/*SPEED SIM
			cout << "Average Total is " <<
			calcAvgTot(total, NUM_TRIALS / 3) << endl;
			SPEED SIM*/ 

			/*DENSITY-FLOW SIM
			cout << "Average Density is " 
					 << calcAvgTot(density, 24) << endl;
			cout << "Average Flow is "
				<< calcAvgTot(flow, 24) << endl;*/

			//emptyLanes(lane, routeLen);
			for (int i = 0; i < (setRuns - 1); i++)
			{
				emptyLanes(lanes[i], routeLen);
			}
			//total == 0;
			finalTotal += total;
		}
//----END SIMULATIONS--------------------------------------------------------//

		//outFile << calcAvgTot(finalTotal, NUM_TRIALS) << endl;
		
		cout << "Average Speed is " << calcAvgTot(finalTotal, NUM_TRIALS * setRuns) << endl;
		cout << "Set Average Density is " << calcAvgTot(density, 18 * 4 * setRuns) << endl;
		cout << "Set Average Flow is " << calcAvgTot(flow, 18 * 4) << endl;
		

		outFile << calcAvgTot(finalTotal, NUM_TRIALS) << " "
			<< calcAvgTot(density, 18 * 4) << " "
			<< calcAvgTot(flow, 18 * 4) << endl;

		finalTotal = 0;
	}
//----END SET OF SIMULATIONS ------------------------------------------------//

	cout << "Done\n";

//----END SIMULATOR----------------------------------------------------------//

	outFile.close();

	return EXIT_SUCCESS;
}

int randSpeed(int min, int max)
{
	int randAdder = randGen(max - min);
	return min + randAdder;
}

void fillLane(Car lane[MAX_LEN], int len, int peopleCars, int selfCars, int totalCars)
{
	const int HIGH_SPEED = MAX_VELOCITY;
	const int LOW_SPEED = 150;
	int randomSpot;

	//assigns cars with people driving
	for (int i = 0; i < peopleCars; i++)
	{
		do
		{
			randomSpot = randGen(len);
		} while (lane[randomSpot].bHasCar = false);

		lane[randomSpot].bHasCar = true;
		lane[randomSpot].bSelfDriving = false;
		lane[randomSpot].updateVelo(randSpeed(LOW_SPEED, HIGH_SPEED), "fillLane");
	}

	//assigns cars that are self-driven
	for (int m = 0; m < selfCars; m++)
	{
		randomSpot = randGen(len);

		do
		{
			randomSpot = randGen(len);
		} while (lane[randomSpot].bHasCar = false);

		lane[randomSpot].bHasCar = true;
		lane[randomSpot].bSelfDriving = true;
		lane[randomSpot].updateVelo(randSpeed(LOW_SPEED, HIGH_SPEED), "fillLane2");
	}

	for (int check = 0; check < totalCars; check++)
	{
		lane[check].getVelo("fillLane");
	}
}

bool hasACar(Car lane[MAX_LEN], int pos)
{
	return lane[pos].bHasCar;
}

int checkVelocity(Car lane[MAX_LEN], int pos)
{
	return lane[pos].getVelo("checkVelocity");
}

bool checkSelfDriving(Car lane[MAX_LEN], int pos)
{
	return lane[pos].bSelfDriving;
}

int randGen(int max)
{
	return rand() % max;
}

void updateCar(Car lane[MAX_LEN], int pos, int len)
{
	const int RANDOM_PROB = 25;
	const int DANGER_PROB = 7;
	const int DANGER_TOTAL = 100000;
	int dangerNum;
	int randomNum;
	int index;
	int prevVelo;

	if (checkSelfDriving(lane, pos))
	{
		//acceleration step
		if (checkVelocity(lane, pos) < MAX_VELOCITY)
		{
			prevVelo = lane[pos].getVelo("updateCar1");
			lane[pos].updateVelo(prevVelo++, "updateCar1");
		}

		//deceleration step
		for (int i = pos + 1; i <= pos + checkVelocity(lane, pos); i++)
		{
			index = i % len;

			if (lane[index].bHasCar == true)
			{
				/*dangerNum = randGen(DANGER_TOTAL);

				if (dangerNum > DANGER_PROB)
				{
				lane[pos].velocity = i - 1;
				}*/

				
				lane[pos].updateVelo(lane[index].getVelo("updateCar2"), "updateCar2"); //speed to distance

				i = pos + lane[pos].getVelo("updateCar3") + 1; //breaks out of loop
			}
		}
	}
	else if (hasACar (lane,pos))
	{
		//acceleration step
		if (checkVelocity(lane, pos) < MAX_VELOCITY)
		{
			lane[pos].updateVelo(lane[pos].getVelo("updateCar4") + 1, "updateCar3");
		}

		//human dangerous random behavior
		if (checkVelocity(lane, pos) < MAX_VELOCITY)
		{
			dangerNum = randGen(DANGER_TOTAL);

			if (dangerNum <= DANGER_PROB)
			{
				lane[pos].updateVelo(lane[pos].getVelo("updateCar5") + 1, "updateCar4");
			}
		}

		//deceleration step
		for (int i = pos + 1; i <= pos + lane[pos].getVelo("updateCar10"); i++)
		{
			index = i % len;

			if (lane[index].bHasCar == true)
			{
				if (lane[pos].getVelo("updateCar8") > lane[index].getVelo("updateCar9"))
				{
					lane[pos].updateVelo(lane[index].getVelo("updateCar5"), "so done"); //drop speed to car ahead
				}

				i = pos + lane[pos].getVelo("updateCar7") + 1; //breaks out of loop
			}
		}

		//randomization step
		randomNum = randGen(100);

		if (randomNum <= RANDOM_PROB)
		{
			lane[pos].updateVelo(lane[pos].getVelo("updateCar8") - 1, "updateCar6");
		}

	}
}

void moveCar (Car lane[MAX_LEN], int len, int flowTotals[NUM_GATES], int routeNum)
{
	int velocity, index;
	Car tempLane[MAX_LEN];

	//move cars to tempLane
	for (int m = 0; m < len; m++)
	{
		if (hasACar (lane, m))
		{
			velocity = lane[m].getVelo("moveCar1");
			index = (m + velocity) % len;

			trackFlow(m, m + velocity, flowTotals, routeNum);
	
			if (!hasACar(tempLane, index))
			{
				tempLane[index].bHasCar = lane[m].bHasCar;
				tempLane[index].bSelfDriving = lane[m].bSelfDriving;
				tempLane[index].updateVelo(lane[m].getVelo ("moveCar2"), "moveCar1");
			}

			tempLane[index].getVelo("moveCar3");
		}
	}

	//cout << "\nInitial lane speed: " << calcAvgSpd(lane, 1, I5);
	//cout << "\nTemp lane speed: " << calcAvgSpd(tempLane, 1, I5);

	//make lane == tempLane 
	for (int n = 0; n < len; n++)
	{
		lane[n].bHasCar = tempLane[n].bHasCar;
		lane[n].bSelfDriving = tempLane[n].bSelfDriving;
		lane[n].updateVelo(tempLane[n].getVelo ("moveCar4"), "moveCar2");
		lane[n].getVelo("moveCar5");
	}
}

double calcDensity(Car lane[MAX_LEN], int gate, int routeNum)
{
	double density;
	int markerHigh;
	int markerLow;
	int count = 0;

	switch (routeNum)
	{
	case I5: 
		switch (gate)
		{
		case 1: markerHigh = I5_1; markerLow = 0;
			break;
		case 2: markerHigh = I5_2; markerLow = I5_1;
			break;
		case 3: markerHigh = I5_3; markerLow = I5_2;
			break;
		case 4: markerHigh = I5_4; markerLow = I5_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case I90: 
		switch (gate)
		{
		case 1: markerHigh = I90_1; markerLow = 0;
			break;
		case 2: markerHigh = I90_2; markerLow = I90_1;
			break;
		case 3: markerHigh = I90_3; markerLow = I90_2;
			break;
		case 4: markerHigh = I90_4; markerLow = I90_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case I405:
		switch (gate)
		{
		case 1: markerHigh = I405_1; markerLow = 0;
			break;
		case 2: markerHigh = I405_2; markerLow = I405_1;
			break;
		case 3: markerHigh = I405_3; markerLow = I405_2;
			break;
		case 4: markerHigh = I405_4; markerLow = I405_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case S520:
		switch (gate)
		{
		case 1: markerHigh = S520_1; markerLow = 0;
			break;
		case 2: markerHigh = S520_2; markerLow = S520_1;
			break;
		case 3: markerHigh = S520_3; markerLow = S520_2;
			break;
		case 4: markerHigh = S520_4; markerLow = S520_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	default: cout << "Error with route number";
		break;
	}

	for (int i = markerLow; i < markerHigh; i++)
	{
		if (hasACar(lane, i))
		{
			count++;
		}
	}

	density = static_cast <double> (count) / (markerHigh - markerLow);

	return density;
}

double calcFlow (int flowTotal)
{
	return static_cast <double> (flowTotal) / 10;
}

void trackFlow(int start, int end, int flowTotals[NUM_GATES], int routeNum)
{
	int marker;

	for (int flowGate = 1; flowGate <= NUM_GATES; flowGate++)
	{
		marker = getMarker(flowGate, routeNum);

		if (start < marker && end > marker)
		{
			flowTotals[flowGate - 1]++;
		}
	}
}

void initializeFlow(int flowTotals[NUM_GATES])
{
	for (int index = 0; index < NUM_GATES; index++)
	{
		flowTotals[index] = 0;
	}
}

int getMarker(int gateNum, int routeNum)
{
	int marker;

	switch (routeNum)
	{
	case I5: 
		switch (gateNum)
		{
		case 1: marker = I5_1;
			break;
		case 2: marker = I5_2;
			break;
		case 3: marker = I5_3;
			break;
		case 4: marker = I5_LEN;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case I90:
		switch (gateNum)
		{
		case 1: marker = I90_1;
			break;
		case 2: marker = I90_2;
			break;
		case 3: marker = I90_3;
			break;
		case 4: marker = I90_LEN;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break; 
	case I405:
		switch (gateNum)
		{
		case 1: marker = I405_1;
			break;
		case 2: marker = I405_2;
			break;
		case 3: marker = I405_3;
			break;
		case 4: marker = I405_LEN;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case S520:
		switch (gateNum)
		{
		case 1: marker = S520_1;
			break;
		case 2: marker = S520_2;
			break;
		case 3: marker = S520_3;
			break;
		case 4: marker = S520_LEN;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	default: cout << "Error with marker"; return EXIT_FAILURE;
		break;
	}

	return marker;
}

double calcAvgSpd(Car lane[MAX_LEN], int gate, int routeNum)
{
	double avgSpd;
	int markerHigh;
	int markerLow;
	int totSpd = 0;
	int count = 0;
	int spdAdder;

	switch (routeNum)
	{
	case I5:
		switch (gate)
		{
		case 1: markerHigh = I5_1; markerLow = 0;
			break;
		case 2: markerHigh = I5_2; markerLow = I5_1;
			break;
		case 3: markerHigh = I5_3; markerLow = I5_2;
			break;
		case 4: markerHigh = I5_4; markerLow = I5_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case I90:
		switch (gate)
		{
		case 1: markerHigh = I90_1; markerLow = 0;
			break;
		case 2: markerHigh = I90_2; markerLow = I90_1;
			break;
		case 3: markerHigh = I90_3; markerLow = I90_2;
			break;
		case 4: markerHigh = I90_4; markerLow = I90_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case I405:
		switch (gate)
		{
		case 1: markerHigh = I405_1; markerLow = 0;
			break;
		case 2: markerHigh = I405_2; markerLow = I405_1;
			break;
		case 3: markerHigh = I405_3; markerLow = I405_2;
			break;
		case 4: markerHigh = I405_4; markerLow = I405_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	case S520:
		switch (gate)
		{
		case 1: markerHigh = S520_1; markerLow = 0;
			break;
		case 2: markerHigh = S520_2; markerLow = S520_1;
			break;
		case 3: markerHigh = S520_3; markerLow = S520_2;
			break;
		case 4: markerHigh = S520_4; markerLow = S520_3;
			break;
		default: cout << "Error with gate number";
			break;
		}
		break;
	default: cout << "Error with route number";
		break;
	}

	for (int pos = markerLow; pos < markerHigh; pos++)
	{
		if (hasACar(lane, pos))
		{
			count++;
			spdAdder = lane[pos].getVelo ("calcAvgSpd");
			totSpd = totSpd + spdAdder;
		}
	}
	
	if (count == 0)
	{
		avgSpd = 0;
	}
	else
	{
		avgSpd = static_cast <double> (totSpd) / count;
	}
	
	return avgSpd;
}

double calcTot(Car lane[MAX_LEN], int routeNum, double &total)
{
	int num = 0;
	double tempTotal = 0;
	double tempAvg;

	for (int index = 0; index < getRoadLength(routeNum); index++)
	{
		if (hasACar(lane, index))
		{
			num++;
			tempTotal += lane[index].getVelo("calcTot");
		}
	}

	tempAvg = tempTotal / num;
	return total += tempAvg;
}

int getRoadLength(int routeNum)
{
	switch (routeNum)
	{
	case I5: return I5_LEN;
		break;
	case I405: return I405_LEN;
		break;
	case I90: return I90_LEN;
		break;
	case S520: return S520_LEN;
		break;
	default: cout << "error with road length"; return EXIT_FAILURE;
		break;
	}
}

double calcAvgTot(double total, double count)
{
	return total / count;
}

void emptyLanes(Car lane[MAX_LEN], int length)
{
	for (int index = 0; index < length; index++)
	{
		lane[index].bHasCar = false;
		lane[index].bSelfDriving = false;
		lane[index].updateVelo(0, "emptyLanes1");
	}
}

void multipleFill(Car lane[5][S520_LEN], int len, int peopleCars, int selfCars, int totalCars, int numLanes)
{
	const int HIGH_SPEED = MAX_VELOCITY;
	const int LOW_SPEED = 150;
	int randomSpot;
	int randomLane;

	//assigns cars with people driving
	for (int i = 0; i < peopleCars; i++)
	{
		randomLane = randGen(numLanes);

		do
		{
			randomSpot = randGen(len);
		} while (lane[randomLane][randomSpot].bHasCar = false);

		lane[randomLane][randomSpot].bHasCar = true;
		lane[randomLane][randomSpot].bSelfDriving = false;
		lane[randomLane][randomSpot].updateVelo(randSpeed(LOW_SPEED, HIGH_SPEED), "fillLane");
	}

	//assigns cars that are self-driven
	for (int m = 0; m < selfCars; m++)
	{
		randomLane = randGen(numLanes);

		do
		{
			randomSpot = randGen(len);
		} while (lane[randomLane][randomSpot].bHasCar = false);

		lane[randomLane][randomSpot].bHasCar = true;
		lane[randomLane][randomSpot].bSelfDriving = true;
		lane[randomLane][randomSpot].updateVelo(randSpeed(LOW_SPEED, HIGH_SPEED), "fillLane2");
	}

	for (int check = 0; check < totalCars; check++)
	{
		lane[randomLane][check].getVelo("fillLane");
	}
}