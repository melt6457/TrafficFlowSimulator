#include "car.h"

const int MAX_VELO = 296;
Car::Car()
{
	bHasCar = false;
	velocity = 0;
	bSelfDriving = false;
}

void Car::updateVelo(int next, string function)
{
	if (next <= MAX_VELO)
	{
		velocity = next;
	}
	else
	{
		cout << "Error with velocity (function: " << function << ")\n";
	}
}

int Car::getVelo(string function)
{
	if (velocity <= MAX_VELO)
	{
		return velocity;
	}
	else
	{
		cout << "Error with getVelo (function: " << function << " " <<
			velocity << ")\n";
		return EXIT_FAILURE;
	}
}