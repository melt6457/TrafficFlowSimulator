#include <iostream>
#include <string>
using namespace std;

class Car
{
private:
	int velocity;
public:
	bool bHasCar;
	bool bSelfDriving;

	//functions
	Car (); //constructor
	void updateVelo(int next, string function);
	int getVelo(string function);
};

